﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Deltas;
using Microsoft.AspNetCore.OData.Formatter;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Results;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using Microsoft.EntityFrameworkCore;
//using System.Web.Http.ModelBinding;
using WebApplication1.Data;
using WebApplication1.Models.QuestionsAndAnswers;
namespace WebApplication1.Controllers.Api
{
    public class ClientConfigurationsController : ODataController
    {
        private ApplicationDbContext _db;
        public ClientConfigurationsController(ApplicationDbContext db)
        {
            _db = db;
        }


        // GET: odata/BranchDics
        [EnableQuery]
        public IQueryable<ClientConfiguration> Get()
        {
            return _db.ClientConfigurations.Where(w => w.Id > 0)
                    .Include(w => w.InvoiceFiles)
                    .Include(w => w.Client);
        }

        // GET: odata/BranchDics(5)
        [EnableQuery]
        public SingleResult<ClientConfiguration> GetSingle([FromODataUri] int key)
        {
            return SingleResult.Create(_db.ClientConfigurations.Where(ClientConfiguration => ClientConfiguration.Id == key));
        }



        // PUT: odata/BranchDics(5)
        public IActionResult Put([FromODataUri] int key, Delta<ClientConfiguration> patch)
        {
            //Validate(patch.GetEntity());

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            ClientConfiguration clientConfiguration = _db.ClientConfigurations.Find(key);
            if (clientConfiguration == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }

            patch.Put(clientConfiguration);

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientConfigurationExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return new OkObjectResult(clientConfiguration);
        }

        // POST: odata/BranchDics
        public IActionResult Post([FromBody] ClientConfiguration clientConfiguration)
        {
            try
            {
                //if (!ModelState.IsValid)
                //{
                //    return BadRequest(ModelState);
                //}
                _db.ClientConfigurations.Add(clientConfiguration);
                _db.SaveChanges();

                return Created(clientConfiguration);

            }
            catch (Exception ex)
            {
                int i = 0;
                return null;
            }
        }

        // PATCH: odata/BranchDics(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IActionResult Patch([FromODataUri] int key, Delta<ClientConfiguration> patch)
        {
            //Validate(patch.GetEntity());

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            ClientConfiguration clientConfiguration = _db.ClientConfigurations.Find(key);
            if (clientConfiguration == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }
            var AllKeys = typeof(Client);
            var properties = AllKeys.GetProperties();
            patch.Patch(clientConfiguration);
            var changed = patch.GetChangedPropertyNames();
            var values = patch.GetInstance();

            try
            {
                _db.SaveChanges();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientConfigurationExists(key))
                {
                    return new StatusCodeResult(StatusCodes.Status404NotFound);
                }
                else
                {
                    throw;
                }
            }

            return Updated(clientConfiguration);
        }

        // DELETE: odata/BranchDics(5)
        public IActionResult Delete([FromODataUri] int key)
        {
            ClientConfiguration clientConfiguration = _db.ClientConfigurations.Find(key);
            if (clientConfiguration == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }

            _db.ClientConfigurations.Remove(clientConfiguration);
            _db.SaveChanges();

            return new StatusCodeResult(StatusCodes.Status204NoContent);
        }

        // GET: odata/Registrations(5)/Classifications
        [EnableQuery]
        public SingleResult<Client> GetClients([FromODataUri] int key)
        {
            return SingleResult.Create(_db.ClientConfigurations.Where(m => m.Id == key).Select(m => m.Client));
        }

        private bool ClientConfigurationExists(int key)
        {
            return _db.ClientConfigurations.Count(e => e.Id == key) > 0;
        }



    }
}
