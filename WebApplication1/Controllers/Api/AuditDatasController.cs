﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.Data;
using WebApplication1.Models.AuditModels;

namespace WebApplication1.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuditDatasController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public AuditDatasController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<AuditData> Get()
        {
            return _dbContext.AuditDatas;
        }
    }
}
