﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Deltas;
using Microsoft.AspNetCore.OData.Formatter;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Results;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using Microsoft.EntityFrameworkCore;
//using System.Web.Http.ModelBinding;
using WebApplication1.Data;
using WebApplication1.Models.QuestionsAndAnswers;
namespace WebApplication1.Controllers.Api
{
    public class FileModelsController : ODataController
    {
        private readonly ApplicationDbContext _db;
        public FileModelsController(ApplicationDbContext db)
        {
            _db = db;
        }


        // GET: odata/BranchDics
        [EnableQuery]
        public IQueryable<FileModel> Get()
        {
            return _db.FileModels.Where(w => w.Id > 0);
        }

        // GET: odata/BranchDics(5)
        [EnableQuery]
        public SingleResult<FileModel> GetSingle([FromODataUri] int key)
        {
            return SingleResult.Create(_db.FileModels.Where(FileModel => FileModel.Id == key));
        }



        // PUT: odata/BranchDics(5)
        public IActionResult Put([FromODataUri] int key, Delta<FileModel> patch)
        {
            //Validate(patch.GetEntity());

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            FileModel FileModel = _db.FileModels.Find(key);
            if (FileModel == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }

            patch.Put(FileModel);

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FileModelExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return new OkObjectResult(FileModel);
        }

        // POST: odata/BranchDics
        public IActionResult Post([FromBody] FileModel FileModel)
        {
            try
            {
                //if (!ModelState.IsValid)
                //{
                //    return BadRequest(ModelState);
                //}
                _db.FileModels.Add(FileModel);
                _db.SaveChanges();

                return Created(FileModel);

            }
            catch (Exception ex)
            {
                int i = 0;
                return null;
            }
        }

        // PATCH: odata/BranchDics(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IActionResult Patch([FromODataUri] int key, Delta<FileModel> patch)
        {
            //Validate(patch.GetEntity());

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            FileModel FileModel = _db.FileModels.Find(key);
            if (FileModel == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }
            var AllKeys = typeof(FileModel);
            var properties = AllKeys.GetProperties();
            patch.Patch(FileModel);
            var changed = patch.GetChangedPropertyNames();
            var values = patch.GetInstance();

            try
            {
                _db.SaveChanges();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FileModelExists(key))
                {
                    return new StatusCodeResult(StatusCodes.Status404NotFound);
                }
                else
                {
                    throw;
                }
            }

            return Updated(FileModel);
        }

        // DELETE: odata/BranchDics(5)
        public IActionResult Delete([FromODataUri] int key)
        {
            FileModel FileModel = _db.FileModels.Find(key);
            if (FileModel == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }

            _db.FileModels.Remove(FileModel);
            _db.SaveChanges();

            return new StatusCodeResult(StatusCodes.Status204NoContent);
        }

        // GET: odata/Registrations(5)/Classifications
        private bool FileModelExists(int key)
        {
            return _db.FileModels.Count(e => e.Id == key) > 0;
        }
    }
}
