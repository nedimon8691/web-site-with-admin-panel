﻿using Microsoft.AspNetCore.OData.Deltas;
using Microsoft.AspNetCore.OData.Formatter;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Results;
using Microsoft.AspNetCore.OData.Routing.Controllers;
//using System.Web.Http.ModelBinding;
using WebApplication1.Data;
using WebApplication1.Models.QuestionsAndAnswers;

namespace WebApplication1.Controllers.Api
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;

    namespace WebApplication.Controllers.Api
    {
        public class ClientConfigurationFileModelsController : ODataController
        {
            private ApplicationDbContext _db;
            public ClientConfigurationFileModelsController(ApplicationDbContext db)
            {
                _db = db;
            }


            // GET: odata/BranchDics
            [EnableQuery]
            public IQueryable<ClientConfigurationFileModel> Get()
            {
                return _db.ClientConfigurationFileModels.Where(w => w.Id > 0);
            }

            // GET: odata/BranchDics(5)
            [EnableQuery]
            public SingleResult<ClientConfigurationFileModel> GetSingle([FromODataUri] int key)
            {
                return SingleResult.Create(_db.ClientConfigurationFileModels.Where(ClientConfigurationFileModel => ClientConfigurationFileModel.Id == key));
            }



            // PUT: odata/BranchDics(5)
            public IActionResult Put([FromODataUri] int key, Delta<ClientConfigurationFileModel> patch)
            {
                //Validate(patch.GetEntity());

                //if (!ModelState.IsValid)
                //{
                //    return BadRequest(ModelState);
                //}

                ClientConfigurationFileModel ClientConfigurationFileModel = _db.ClientConfigurationFileModels.Find(key);
                if (ClientConfigurationFileModel == null)
                {
                    return new StatusCodeResult(StatusCodes.Status404NotFound);
                }

                patch.Put(ClientConfigurationFileModel);

                try
                {
                    _db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientConfigurationFileModelExists(key))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return new OkObjectResult(ClientConfigurationFileModel);
            }

            // POST: odata/BranchDics
            public IActionResult Post([FromBody] ClientConfigurationFileModel ClientConfigurationFileModel)
            {
                try
                {
                    //if (!ModelState.IsValid)
                    //{
                    //    return BadRequest(ModelState);
                    //}
                    //registration.Id = _db.Registration.Max(m => m.Id) + 1;
                    _db.ClientConfigurationFileModels.Add(ClientConfigurationFileModel);
                    _db.SaveChanges();

                    return Created(ClientConfigurationFileModel);

                }
                catch (Exception ex)
                {
                    int i = 0;
                    return null;
                }
            }

            // PATCH: odata/BranchDics(5)
            [AcceptVerbs("PATCH", "MERGE")]
            public IActionResult Patch([FromODataUri] int key, Delta<ClientConfigurationFileModel> patch)
            {
                //Validate(patch.GetEntity());

                //if (!ModelState.IsValid)
                //{
                //    return BadRequest(ModelState);
                //}

                ClientConfigurationFileModel ClientConfigurationFileModel = _db.ClientConfigurationFileModels.Find(key);
                if (ClientConfigurationFileModel == null)
                {
                    return new StatusCodeResult(StatusCodes.Status404NotFound);
                }
                var AllKeys = typeof(Client);
                var properties = AllKeys.GetProperties();
                patch.Patch(ClientConfigurationFileModel);
                var changed = patch.GetChangedPropertyNames();
                var values = patch.GetInstance();

                try
                {
                    _db.SaveChanges();

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientConfigurationFileModelExists(key))
                    {
                        return new StatusCodeResult(StatusCodes.Status404NotFound);
                    }
                    else
                    {
                        throw;
                    }
                }

                return Updated(ClientConfigurationFileModel);
            }

            // DELETE: odata/BranchDics(5)
            public IActionResult Delete([FromODataUri] int key)
            {
                ClientConfigurationFileModel ClientConfigurationFileModel = _db.ClientConfigurationFileModels.Find(key);
                if (ClientConfigurationFileModel == null)
                {
                    return new StatusCodeResult(StatusCodes.Status404NotFound);
                }

                _db.ClientConfigurationFileModels.Remove(ClientConfigurationFileModel);
                _db.SaveChanges();

                return new StatusCodeResult(StatusCodes.Status204NoContent);
            }

            // GET: odata/Registrations(5)/Classifications

            private bool ClientConfigurationFileModelExists(int key)
            {
                return _db.ClientConfigurationFileModels.Count(e => e.Id == key) > 0;
            }



        }
    }
}
