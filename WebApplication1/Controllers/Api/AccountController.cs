﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using System.Text;
using WebApplication1.Data;
using WebApplication1.Models.IdentityModels;
using WebApplication1.Services.AuditServices;
using WebApplication1.Models.QuestionsAndAnswers;

namespace WebApplication1.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private ApplicationDbContext _context;
        public IAuditService _auditService;
        public string _remoteIpAddress;
        //public ISendEmail _sendEmail;

        public AccountController(UserManager<ApplicationUser> userManager, 
                                 SignInManager<ApplicationUser> signInManager, 
                                 ApplicationDbContext context, 
                                 IAuditService auditService)//, ISendEmail sendEmai)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _auditService = auditService;
            //_sendEmail = sendEmai;
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult LoginClient()
        {
            return View();
        }
        

        [AllowAnonymous]
        public IActionResult VerificationCode()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser { Email = model.Email, UserName = model.Email, External = true};
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    var newClient = new Client()
                    {
                        Email = model.Email,
                        Name = model.Email,
                        ClientNumber = GenerateClientNumber(),
                        Appproved = false,

                    };
                    _context.Client.Add(newClient);
                    _context.SaveChanges();

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Registration(RegistrationModelDTO model)
        {
            _remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            if (ModelState.IsValid)
            {
                var newUser = new ApplicationUser()
                {
                    Email = model.Email,
                    UserName = model.Username,
                };

                var result = await _userManager.CreateAsync(newUser, model.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(newUser, false);
                    await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "New user registration", true, null);
                    return Ok();
                }
                else
                {
                    await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "New user registration", false, result.ToString());
                    return BadRequest();
                }
            }
            else
            {
                await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "New user registration", false, "Registration error");
                return BadRequest();
            }


        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginModelDTO model)
        {
            _remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, false);
                
                if (result.Succeeded)
                {
                    var user = _context.ApplcationUsers.First(u => u.UserName == model.Username);
                    await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "LogIn", false, "success");
                    
                    if (_userManager.GetRolesAsync(user).Result.Contains("Administrator"))
                        return RedirectToAction("Admin", "WorkPlace");
                    if (_userManager.GetRolesAsync(user).Result.Contains("Manager"))
                        return RedirectToAction("Manager", "WorkPlace");
                    
                    if (_userManager.GetRolesAsync(user).Result.Contains("Operator"))
                        return RedirectToAction("Operator", "WorkPlace");

                    return RedirectToAction("Index", "Home");
                }
                //else
                //{
                //    await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "LogIn", false, "Incorrect credentials");
                //    string message = "Wrong login or password";
                //    return RedirectToAction("ErrorPage", "Home", new { message = message });
                //}
            }
            await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "LogIn", false, "Incorrect credentials");
            string message = "Wrong login or password";
            return RedirectToAction("ErrorPage", "Home", new { message = message });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> LoginClient(LoginModelDTO model)
        {
            //Random random = new Random();
            _remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, false);

                if (result.Succeeded)
                {
                    await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "LogIn", false, "success");
                    return RedirectToAction("Index", "Home");
                }
            }
            await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "LogIn", false, "Incorrect credentials");
            string message = "Wrong login or password";
            return RedirectToAction("ErrorPage", "Home", new { message = message });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> VerifyCode(string code)
        {
            _remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            if (TempData["userLogin"] != null && TempData["userPass"] != null && code != null)
            {
                LoginModelDTO model = new LoginModelDTO();
                model.Username = TempData["userLogin"] as string;
                model.Password = TempData["userPass"] as string;

                var incomingCodeHash = CalculateHash(code);
                var user = await _userManager.FindByNameAsync(model.Username);
                var result = String.Compare(incomingCodeHash, user.AccessToken);

                if (result != 0)
                {
                    string message = "Invalid verification code";
                    return RedirectToAction("ErrorPage", "Home", new { message = message });
                }

                var loginResult = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, false);

                if (loginResult.Succeeded)
                {
                    user.AccessToken = null;
                    await _userManager.UpdateAsync(user);
                    await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "LogIn", true, null);

                    if (User.IsInRole("Administrator"))
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    else if (User.IsInRole("InformationSecurity"))
                    {
                        return RedirectToAction("Audit", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    await _auditService.CreateAuditASync(DateTime.Now, model.Username, _remoteIpAddress, "Users", "LogIn", false, "Incorrect credentials. Verify code step");
                    return RedirectToAction("Login", "Account");
                }
            }
            else
            { 
                
            }

            return null;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Logout()
        {
            _remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            try
            {
                await _signInManager.SignOutAsync();
                await _auditService.CreateAuditASync(DateTime.Now, User.Identity.Name, _remoteIpAddress, "Users", "LogOut", true, null);
                if (User.IsInRole("Adminitrator") || User.IsInRole("Operator") || User.IsInRole("Manager"))
                    return RedirectToAction("Login", "Account");
                else
                    return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                await _auditService.CreateAuditASync(DateTime.Now, User.Identity.Name, _remoteIpAddress, "Users", "LogOut", true, ex.Message);
                return RedirectToAction("Error", "Home");
            }

        }


        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddRole(RoleDTO paramRoleDTO)
        {
            _remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            try
            {
                if (paramRoleDTO == null)
                {
                    return BadRequest();
                }

                var RoleName = paramRoleDTO.RoleName.Trim();

                if (RoleName == "")
                {
                    throw new Exception("No RoleName");
                }

                // Create Role
                var roleManager = _context.Roles;

                var waited = await roleManager.FirstOrDefaultAsync(f => f.Name == RoleName);
                if (waited == null)
                {
                    await roleManager.AddAsync(new IdentityRole(RoleName));
                    _ = _context.SaveChangesAsync();
                }
                await _auditService.CreateAuditASync(DateTime.Now, User.Identity.Name, _remoteIpAddress, "UserRoles", "Create role", true, null);
                return Ok();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                await _auditService.CreateAuditASync(DateTime.Now, User.Identity.Name, _remoteIpAddress, "UserRoles", "Create role", false, ex.Message);
                return Redirect("/Mode/Index");
            }
        }


        public string CalculateHash(string input)
        {
            using (var algorithm = SHA512.Create()) //or MD5 SHA256 etc.
            {
                var hashedBytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(input));

                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }

        public string GenerateClientNumber()
        {
            int CheckNumber = 0;
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder builder = new StringBuilder();
            string NewNumber = "";

            do
            {
                var ranStr = (Enumerable.Repeat(chars, 2).Select(s => s[random.Next(s.Length)]).ToArray());
                int ranInt = random.Next(1, 9999);

                builder.Append(ranStr);
                builder.Append(ranInt);
                NewNumber = builder.ToString();
                CheckNumber = _context.Client.Where(w => w.ClientNumber == NewNumber).Count();
                CheckNumber = CheckNumber == 0 ? 0 : 1;
            }
            while (CheckNumber == 1);

            return NewNumber;
        }
    }
}
