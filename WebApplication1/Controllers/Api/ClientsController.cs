﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Deltas;
using Microsoft.AspNetCore.OData.Formatter;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Results;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using Microsoft.EntityFrameworkCore;
//using System.Web.Http.ModelBinding;
using WebApplication1.Data;
using WebApplication1.Models.QuestionsAndAnswers;

namespace WebApplication1.Controllers.Api
{
    public class ClientsController : ODataController
    {
        private ApplicationDbContext _db;
        public ClientsController(ApplicationDbContext db)
        {
            _db = db;
        }


        // GET: odata/BranchDics
        [EnableQuery]
        public IQueryable<Client> Get()
        {
            return _db.Client.Where(w => w.Id > 0);
        }

        // GET: odata/BranchDics(5)
        [EnableQuery]
        public SingleResult<Client> GetSingle([FromODataUri] int key)
        {
            return SingleResult.Create(_db.Client.Where(Client => Client.Id == key));
        }



        // PUT: odata/BranchDics(5)
        public IActionResult Put([FromODataUri] int key, Delta<Client> patch)
        {
            //Validate(patch.GetEntity());

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            Client client = _db.Client.Find(key);
            if (client == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }

            patch.Put(client);

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientnExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return new OkObjectResult(client);
        }

        // POST: odata/BranchDics
        public IActionResult Post([FromBody] Client client)
        {
            try
            {
                //if (!ModelState.IsValid)
                //{
                //    return BadRequest(ModelState);
                //}
                _db.Client.Add(client);
                _db.SaveChanges();

                return Created(client);

            }
            catch (Exception ex)
            {
                int i = 0;
                return null;
            }
        }

        // PATCH: odata/BranchDics(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IActionResult Patch([FromODataUri] int key, Delta<Client> patch)
        {
            //Validate(patch.GetEntity());

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            Client client = _db.Client.Find(key);
            if (client == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }
            var AllKeys = typeof(Client);
            var properties = AllKeys.GetProperties();
            patch.Patch(client);
            var changed = patch.GetChangedPropertyNames();
            var values = patch.GetInstance();

            try
            {
                _db.SaveChanges();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientnExists(key))
                {
                    return new StatusCodeResult(StatusCodes.Status404NotFound);
                }
                else
                {
                    throw;
                }
            }

            return Updated(client);
        }

        // DELETE: odata/BranchDics(5)
        public IActionResult Delete([FromODataUri] int key)
        {
            Client client = _db.Client.Find(key);
            if (client == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }

            _db.Client.Remove(client);
            _db.SaveChanges();

            return new StatusCodeResult(StatusCodes.Status204NoContent);
        }

        [EnableQuery]
        public IQueryable<ClientConfiguration> GetClientConfiguration([FromODataUri] int key)
        {
            return _db.Client.Where(m => m.Id == key).SelectMany(m => m.ClientConfigurations);
        }

        private bool ClientnExists(int key)
        {
            return _db.Client.Count(e => e.Id == key) > 0;
        }
    }
}
