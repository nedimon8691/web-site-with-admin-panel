﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Deltas;
using Microsoft.AspNetCore.OData.Formatter;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Results;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using Microsoft.EntityFrameworkCore;
//using System.Web.Http.ModelBinding;
using WebApplication1.Data;
using WebApplication1.Models.QuestionsAndAnswers;

namespace WebApplication1.Controllers.Api
{
    public class TariffsController : ODataController
    {
        private readonly ApplicationDbContext _db;
        public TariffsController(ApplicationDbContext db)
        {
            _db = db;
        }


        // GET: odata/BranchDics
        [EnableQuery]
        public IQueryable<Tariff> Get()
        {
            return _db.Tariffs.Where(w => w.Id > 0);
        }

        // GET: odata/BranchDics(5)
        [EnableQuery]
        public SingleResult<Tariff> GetSingle([FromODataUri] int key)
        {
            return SingleResult.Create(_db.Tariffs.Where(Tariff => Tariff.Id == key));
        }



        // PUT: odata/BranchDics(5)
        public IActionResult Put([FromODataUri] int key, Delta<Tariff> patch)
        {
            //Validate(patch.GetEntity());

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            Tariff Tariff = _db.Tariffs.Find(key);
            if (Tariff == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }

            patch.Put(Tariff);

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TariffExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return new OkObjectResult(Tariff);
        }

        // POST: odata/BranchDics
        public IActionResult Post([FromBody] Tariff Tariff)
        {
            try
            {
                //if (!ModelState.IsValid)
                //{
                //    return BadRequest(ModelState);
                //}
                _db.Tariffs.Add(Tariff);
                _db.SaveChanges();

                return Created(Tariff);

            }
            catch (Exception ex)
            {
                int i = 0;
                return null;
            }
        }

        // PATCH: odata/BranchDics(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IActionResult Patch([FromODataUri] int key, Delta<Tariff> patch)
        {
            //Validate(patch.GetEntity());

            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            Tariff Tariff = _db.Tariffs.Find(key);
            if (Tariff == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }
            var AllKeys = typeof(Tariff);
            var properties = AllKeys.GetProperties();
            patch.Patch(Tariff);
            var changed = patch.GetChangedPropertyNames();
            var values = patch.GetInstance();

            try
            {
                _db.SaveChanges();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TariffExists(key))
                {
                    return new StatusCodeResult(StatusCodes.Status404NotFound);
                }
                else
                {
                    throw;
                }
            }

            return Updated(Tariff);
        }

        // DELETE: odata/BranchDics(5)
        public IActionResult Delete([FromODataUri] int key)
        {
            Tariff Tariff = _db.Tariffs.Find(key);
            if (Tariff == null)
            {
                return new StatusCodeResult(StatusCodes.Status404NotFound);
            }

            _db.Tariffs.Remove(Tariff);
            _db.SaveChanges();

            return new StatusCodeResult(StatusCodes.Status204NoContent);
        }

        // GET: odata/Registrations(5)/Classifications
        private bool TariffExists(int key)
        {
            return _db.Tariffs.Count(e => e.Id == key) > 0;
        }
    }
}
