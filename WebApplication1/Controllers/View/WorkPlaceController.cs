﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace WebApplication1.Controllers.View
{
    [Authorize(Roles = "Administrator, Operator, Manager")]
    public class WorkPlaceController : Controller
    {
        public IActionResult Index()
        {
            if (User.IsInRole("Administrator"))
               return RedirectToAction(nameof(Admin));
            if (User.IsInRole("Operator"))
               return RedirectToAction(nameof(Operator));
            if (User.IsInRole("Manager"))
               return RedirectToAction(nameof(Manager));

            return NotFound();
        }
        public IActionResult Admin()
        {
            return View();
        }
        public IActionResult Manager()
        {
            return View();
        }
        public IActionResult Operator()
        {
            return View();
        }
    }
}
