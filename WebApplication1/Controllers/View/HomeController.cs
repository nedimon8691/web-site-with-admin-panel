﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Security.Claims;
using System.Text;
using WebApplication1.Data;
using WebApplication1.DtoModels;
using WebApplication1.Models;
using WebApplication1.Models.QuestionsAndAnswers;
using WebApplication1.Services.EncryptionServices;
using WebApplication1.Utility;

namespace WebApplication1.Controllers
{
    //[Authorize]
    [Route("[controller]/[action]")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _dbContext;
        private readonly IEncryptionService _encryptionService;
        private readonly string? userId;
        public HomeController(ILogger<HomeController> logger, 
                              IEncryptionService encryptionService, 
                              IHttpContextAccessor httpContextAccessor,
                              ApplicationDbContext dbContext)
        {
            _logger = logger;
            _encryptionService = encryptionService;
            _dbContext = dbContext; 
            userId = httpContextAccessor.HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        }

        public IActionResult Instruction()
        {
            return View();  
        }

        public IActionResult Tariff()
        {
            var data = _dbContext.Tariffs.Where(w => w.IsDeleted == false);
            return View(data);  
        }

        public IActionResult Payment()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            if (User.IsInRole("Administrator"))
            {
                return RedirectToAction("Admin","Workplace");
            }
            if (User.IsInRole("Employee"))
            {
                return RedirectToAction("Manager", "Workplace");
            }
            if (User.IsInRole("Operator"))
            {
                return RedirectToAction("Operator", "Workplace");
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult MyPage()
        {
            var user = _dbContext.Users.First(f => f.Id == userId);
            var client = _dbContext.Client.Include(c => c.ClientConfigurations).First(c => c.Email == user.Email);
            return View(client);
        }
        [HttpPost]
        public async Task<IActionResult> Create(string name, string deviceId,int tariffId)
        {
            var tariff = _dbContext.Tariffs.First(t => t.Id == tariffId);
            var ClientConfiguration = new ClientConfiguration()
            {
                DeviceId = deviceId,
                Price = tariff.Price,
                DateEnd = DateTime.Now.AddMonths(tariff.Months ?? 0),
                PaymentStatus = PaymentStatus.Initial
            };

            var user = _dbContext.Users.First(u => u.Id == userId);
            var client = _dbContext.Client.First(c => c.Email == user.Email);
            ClientConfiguration.ClientId = client.Id;

            await _dbContext.AddAsync(ClientConfiguration);
            await _dbContext.SaveChangesAsync();

            return Redirect(nameof(MyPage));
        }
       

        [HttpGet]
        public IActionResult UploadPaycheck(int id)
        {
            return View(_dbContext.ClientConfigurations.First(u => u.Id == id));
        }
        

        public IActionResult Create()
        {
            ViewBag.Tariffs = _dbContext.Tariffs.ToArray();
            return View(new PaymentUnit());
        }

        public IActionResult DownloadInstaller()
        {
            FileStream file = System.IO.File.OpenRead("Files/AboutMe.txt");
            return File(file, "application/x-msdownload", "AboutMe.txt");
        }
       
        public IActionResult GetLicenseFile(int id)
        {
            var licenseData = _dbContext.ClientConfigurations.First(cc => cc.Id == id);
            var file = _encryptionService.GetLicenseFile(licenseData);

            return File(file, "application/octet-stream", "license");
        }
        public async Task<IActionResult> UploadDeviceIdFile([FromQuery] int clientConfigurationId, IFormFile file)
        {
            var deviceId = Encoding.UTF8.GetString(file.OpenReadStream().ToArray());
            var config = _dbContext.ClientConfigurations.First(cc => cc.Id == clientConfigurationId);
            config.DeviceId = deviceId;
            
            _dbContext.Update(config);
            await _dbContext.SaveChangesAsync();
            
            return Ok();
        }


        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> UploadPaycheck(int clientConfigurationId, List<IFormFile> invoiceFiles)
        {
            byte[] buffer = new byte[16 * 1024];
            var files = new List<FileModel>();
            
            foreach(var fileForm in invoiceFiles)
            {
                using var fileStream = fileForm.OpenReadStream();
                using MemoryStream ms = new MemoryStream();
                int read;
                while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                FileModel entity = new FileModel
                {
                    FileContent = ms.ToArray(),
                    FileName = fileForm.FileName,
                };
                _dbContext.Add(entity);
                files.Add(entity);
            }

            await  _dbContext.SaveChangesAsync();

            var config = _dbContext.ClientConfigurations.First(cc => cc.Id == clientConfigurationId);
            
            config.PaymentStatus = PaymentStatus.Waiting;

            var clientConfigurationFiles = files.Select(f => new ClientConfigurationFileModel
            {
                FileModelId = f.Id,
                ClientConfigurationId = clientConfigurationId,
            }).ToArray();
            //foreach (var file in files)
            //{
            //    var newConfFiles = new ClientConfigurationFileModel();
            //    newConfFiles.FileModelId = file.Id;
            //    newConfFiles.ClientConfigurationId = clientConfigurationId;
            //    _dbContext.Add(newConfFiles);
            //}
            //_dbContext.SaveChanges();

            //var conf = await _dbContext.ClientConfigurationFileModels
            //    .Where(w => w.ClientConfigurationId == clientConfigurationId).ToListAsync();
            try
            {
                await _dbContext.AddRangeAsync(clientConfigurationFiles);

                config.InvoiceFiles = clientConfigurationFiles;

                _dbContext.Update(config);
                await _dbContext.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                int r = 0;
            }


            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> DownloadPaycheck([FromQuery]int fileId)
        {
            var file = _dbContext.FileModels.First(f => f.Id == fileId);
            MimeTypes.TryGetMimeType(file.FileName, out var mimeType);
            
            return File(file.FileContent, mimeType, file.FileName);
        }

        [AllowAnonymous]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [AllowAnonymous]
        public IActionResult ErrorPage(string message)
        {
            if (message == null)
            {
                ViewBag.Error = "An error has occurred. Return to the page and try again";
            }
            else
                ViewBag.Error = message;
            
            return View();
        }
      
        
        public IActionResult Audit()
        {
            return View();
        }
    }
}
