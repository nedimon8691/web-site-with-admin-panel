﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication1.Models.IdentityModels;
using WebApplication1.Data;
using X.PagedList;
using WebApplication1.Models.QuestionsAndAnswers;

namespace WebApplication1.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Route("[controller]/[action]")]
    public class AdminController : Controller
    {
        private ApplicationDbContext _db;
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;
        public AdminController(UserManager<ApplicationUser> manager, 
                               RoleManager<IdentityRole> roleManager, 
                               ApplicationDbContext db)
        {
            _db = db;
            _userManager = manager;
            _roleManager = roleManager;
        }
        #region public IActionResult MainUsers(string searchStringUserNameOrEmail)
        public async Task<ActionResult> Index(string searchStringUserNameOrEmail, string currentFilter, int? page)
        {
            try
            {
                int intPage = 1;
                int intPageSize = 15;
                int intTotalPageCount = 0;

                if (searchStringUserNameOrEmail != null)
                {
                    intPage = 1;
                }
                else
                {
                    if (currentFilter != null)
                    {
                        searchStringUserNameOrEmail = currentFilter;
                        intPage = page ?? 1;
                    }
                    else
                    {
                        searchStringUserNameOrEmail = "";
                        intPage = page ?? 1;
                    }
                }

                ViewBag.CurrentFilter = searchStringUserNameOrEmail;

                List<ExpandedUserDTO> col_UserDTO = new List<ExpandedUserDTO>();
                int intSkip = (intPage - 1) * intPageSize;

                intTotalPageCount = _userManager.Users
                    .Where(x => x.UserName.Contains(searchStringUserNameOrEmail) && x.Deleted != true && x.External == false)
                    .Count();

                var result = _userManager.Users
                    .Where(x => x.UserName.Contains(searchStringUserNameOrEmail) && x.Deleted != true && x.External == false)
                    .OrderBy(x => x.UserName)
                    .Skip(intSkip)
                    .Take(intPageSize)
                    .ToList();

                foreach (var item in result)
                {
                    ExpandedUserDTO objUserDTO = new ExpandedUserDTO();
                    objUserDTO.UserName = item.UserName;
                    objUserDTO.Email = item.Email;

                    col_UserDTO.Add(objUserDTO);
                }

                // Set the number of pages
                var _UserDTOAsIPagedList =
                    new StaticPagedList<ExpandedUserDTO>
                    (
                        col_UserDTO, intPage, intPageSize, intTotalPageCount
                        );

                return View(_UserDTOAsIPagedList);
            }
            catch (Exception ex)
            {
                var rr = ex;
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                List<ExpandedUserDTO> col_UserDTO = new List<ExpandedUserDTO>();

                return View(col_UserDTO.ToPagedList(1, 25));

            }
        }
        #endregion

        // Users *****************************

        // PUT: /Admin/Create

        #region public async Task<IActionResult> Create(ExpandedUserDTO paramExpandedUserDTO)

     
        public async Task<ActionResult> Create()
        {
            ExpandedUserDTO objExpandedUserDTO = new ExpandedUserDTO();
            ViewBag.Roles = _db.Roles.ToList();

            return View(objExpandedUserDTO);
        }

        [HttpPost]
        public async Task<IActionResult> Create(ExpandedUserDTO paramExpandedUserDTO)
        {
            try
            {
                if (paramExpandedUserDTO == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                var checkUserName = await _userManager.FindByNameAsync(paramExpandedUserDTO.UserName);
                if (checkUserName != null)
                {
                    string message = "The name has already been used. Please use another one";
                    return RedirectToAction("ErrorPage", "Home", new { message = message });
                }

                var Email = paramExpandedUserDTO.Email;
                var UserName = paramExpandedUserDTO.UserName;
                var Password = paramExpandedUserDTO.Password;
                var Role = paramExpandedUserDTO.Role;

                if (Email == "" || Email == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                if (UserName == "" || UserName == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                if (Password == "" || Password == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                var objNewAdminUser = new ApplicationUser
                {
                    UserName = UserName,
                    Email = Email,
                    External = false,
                    SecondName = paramExpandedUserDTO.SecondName,
                    FirstName = paramExpandedUserDTO.FirstName,
                    BirthDate = paramExpandedUserDTO.BirthDate
                };
                var AdminUserCreateResult = await _userManager.CreateAsync(objNewAdminUser, Password);

                if (AdminUserCreateResult.Succeeded == true)
                {
                    if (Role != null)
                        await _userManager.AddToRoleAsync(objNewAdminUser, Role);
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("ErrorPage", "Home");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Roles = GetAllRolesAsSelectList();
                return RedirectToAction("ErrorPage", "Home");
            }
        }
        #endregion

        #region private void DeleteUser(string UserName)
        public async Task<ActionResult> DeleteUser(string UserName)
        {
            ApplicationUser user =
                _userManager.FindByNameAsync(UserName).Result;

            if (user == null)
            {
                throw new Exception("Could not find the User");
            }
            user.DeletedAt = DateTime.Now;
            user.Deleted = true;
            await _userManager.UpdateAsync(user);
            return RedirectToAction("Index");
        }
        #endregion

        // PUT: /Admin/EditUser
        #region public async Task<ActionResult> EditUser(ExpandedUserDTO pass)
        public async Task<ActionResult> EditUser(string UserName)
        {
            if (UserName == null)
            {
                return BadRequest();
            }
            ExpandedUserDTO objExpandedUserDTO = GetUser(UserName);
            if (objExpandedUserDTO == null)
            {
                return NotFound();
            }
            return View(objExpandedUserDTO);
        }
        [HttpPost]
        public async Task<ActionResult> EditUser(ExpandedUserDTO paramExpandedUserDTO)
        {
            if (paramExpandedUserDTO == null)
            {
                return BadRequest();
            }


            try
            {
                ApplicationUser result = await  _userManager.FindByNameAsync(paramExpandedUserDTO.UserName);
                result.Email = paramExpandedUserDTO.Email;
                if (await _userManager.IsLockedOutAsync(result))
                {
                    // Unlock user
                    await _userManager.ResetAccessFailedCountAsync(result);
                }

                await _userManager.UpdateAsync(result);
                if (!string.IsNullOrEmpty(paramExpandedUserDTO.Password))
                {
                    // Remove current password
                    await _userManager.RemovePasswordAsync(result);
                    await _userManager.AddPasswordAsync(result, paramExpandedUserDTO.Password);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home");
            }

            return RedirectToAction("Index");
        }
        #endregion

        // PUT: /Admin/EditRoles/TestUser 

        #region public async Task<ActionResult> EditRoles(UserAndRolesDTO paramUserAndRolesDTO)
        public async Task<ActionResult> EditRoles(string UserName)
        {
            if (UserName == null)
            {
                return BadRequest();
            }

            UserName = UserName.ToLower();

            // Check that we have an actual user
            ExpandedUserDTO objExpandedUserDTO = GetUser(UserName);

            if (objExpandedUserDTO == null)
            {
                return NotFound();
            }

            UserAndRolesDTO objUserAndRolesDTO =
                GetUserAndRoles(UserName);

            return View(objUserAndRolesDTO);
        }

        [HttpPost]
        public async Task<ActionResult> EditRoles(UserAndRolesDTO paramUserAndRolesDTO)
        {
            try
            {
                if (paramUserAndRolesDTO == null)
                {
                    return new StatusCodeResult(StatusCodes.Status400BadRequest);
                }

                string UserName = paramUserAndRolesDTO.UserName;
                string strNewRole = Convert.ToString(Request.Form["AddRole"]);

                if (strNewRole != "No roles")
                {
                    // Go get the User
                    ApplicationUser user = _userManager.FindByNameAsync(UserName).Result;

                    // Put user in role
                    await _userManager.AddToRoleAsync(user, strNewRole);
                }

                ViewBag.AddRole = new SelectList(RolesUserIsNotIn(UserName));

                UserAndRolesDTO objUserAndRolesDTO =
                    GetUserAndRoles(UserName);

                return View(objUserAndRolesDTO);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                return View("EditRoles");
            }
        }
        #endregion

        // DELETE: /Admin/DeleteRole?UserName="TestUser&RoleName=Administrator
        #region public async Task<ActionResult> DeleteRole(string UserName, string RoleName)
        public async Task<ActionResult> DeleteRole(string UserName, string RoleName)
        {
            try
            {
                if ((UserName == null) || (RoleName == null))
                {
                    return new StatusCodeResult(StatusCodes.Status400BadRequest);
                }

                UserName = UserName.ToLower();

                // Check that we have an actual user
                ExpandedUserDTO objExpandedUserDTO = GetUser(UserName);

                if (objExpandedUserDTO == null)
                {
                    return NotFound();
                }

                if (UserName.ToLower() ==
                    this.User.Identity.Name.ToLower() && RoleName == "Administrator")
                {
                    ModelState.AddModelError(string.Empty,
                        "Error: Cannot delete Administrator Role for the current user");
                }

                // Go get the User
                ApplicationUser user = _userManager.FindByNameAsync(UserName).Result;
                // Remove User from role
                await _userManager.RemoveFromRoleAsync(user, RoleName);
                await _userManager.UpdateAsync(user);

                ViewBag.AddRole = new SelectList(RolesUserIsNotIn(UserName));

                return RedirectToAction("EditRoles", new { UserName = UserName });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);

                ViewBag.AddRole = new SelectList(RolesUserIsNotIn(UserName));

                UserAndRolesDTO objUserAndRolesDTO =
                    GetUserAndRoles(UserName);

                return View("EditRoles", objUserAndRolesDTO);
            }
        }
        #endregion

        // Roles *****************************

        // GET: /Admin/ViewAllRoles
        #region public async Task<ActionResult> ViewAllRoles()
        public async Task<ActionResult> ViewAllRoles()
        {
            List<RoleDTO> colRoleDTO = (from objRole in _roleManager.Roles
                                        select new RoleDTO
                                        {
                                            Id = objRole.Id,
                                            RoleName = objRole.Name
                                        }).ToList();

            return View(colRoleDTO);
        }
        #endregion

        // PUT: /Admin/AddRole
        #region public async Task<ActionResult> AddRole(RoleDTO paramRoleDTO)

        public async Task<ActionResult> AddRole()
        {
            RoleDTO objRoleDTO = new RoleDTO();

            return View(objRoleDTO);
        }
        [HttpPost]
        public async Task<ActionResult> AddRole(RoleDTO paramRoleDTO)
        {
            try
            {
                if (paramRoleDTO == null)
                {
                    return new StatusCodeResult(StatusCodes.Status400BadRequest);
                }

                var RoleName = paramRoleDTO.RoleName;

                if (RoleName == "")
                {
                    throw new Exception("No RoleName");
                }

                if (!_roleManager.RoleExistsAsync(RoleName).Result)
                {
                    await _roleManager.CreateAsync(new IdentityRole(RoleName));
                }

                return RedirectToAction("ViewAllRoles");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);
                return BadRequest(ModelState);
            }
        }
        #endregion

        // DELETE: /Admin/DeleteUserRole?RoleName=TestRole
        #region public async Task<ActionResult> DeleteUserRole(string RoleName)
        public async Task<ActionResult> DeleteUserRole(string RoleName)
        {
            //RoleCheck();
            try
            {
                if (RoleName == null)
                {
                    return new StatusCodeResult(StatusCodes.Status400BadRequest);
                }

                if (RoleName.ToLower() == "administrator")
                {
                    throw new Exception(String.Format("Cannot delete {0} Role.", RoleName));
                }

                var UsersInRole = _userManager.GetUsersInRoleAsync(RoleName).Result.Count();
                if (UsersInRole > 0)
                {
                    throw new Exception(
                        String.Format(
                            "Cannot delete {0} Role because it still has users.",
                            RoleName)
                            );
                }

                var objRoleToDelete = (from objRole in _roleManager.Roles
                                       where objRole.Name == RoleName
                                       select objRole).FirstOrDefault();
                if (objRoleToDelete != null)
                {
                    await _roleManager.DeleteAsync(objRoleToDelete);
                }
                else
                {
                    throw new Exception(
                        String.Format(
                            "Canot delete {0} Role does not exist.",
                            RoleName)
                            );
                }

                List<RoleDTO> colRoleDTO = (from objRole in _roleManager.Roles
                                            select new RoleDTO
                                            {
                                                Id = objRole.Id,
                                                RoleName = objRole.Name
                                            }).ToList();

                return View("ViewAllRoles", colRoleDTO);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error: " + ex);

                List<RoleDTO> colRoleDTO = (from objRole in _roleManager.Roles
                                            select new RoleDTO
                                            {
                                                Id = objRole.Id,
                                                RoleName = objRole.Name
                                            }).ToList();

                return View("ViewAllRoles", colRoleDTO);
            }
        }
        #endregion
        #region private List<SelectListItem> GetAllRolesAsSelectList()
        private List<SelectListItem> GetAllRolesAsSelectList()
        {
            List<SelectListItem> SelectRoleListItems =
                new List<SelectListItem>();

            var colRoleSelectList = _roleManager.Roles.OrderBy(x => x.Name).ToList();

            SelectRoleListItems.Add(
                new SelectListItem
                {
                    Text = "Выбор",
                    Value = "0"
                });

            foreach (var item in colRoleSelectList)
            {
                SelectRoleListItems.Add(
                    new SelectListItem
                    {
                        Text = item.Name.ToString(),
                        Value = item.Name.ToString()
                    });
            }

            return SelectRoleListItems;
        }
        #endregion

        #region private ExpandedUserDTO GetUser(string paramUserName)
        private ExpandedUserDTO GetUser(string paramUserName)
        {
            ExpandedUserDTO objExpandedUserDTO = new ExpandedUserDTO();

            var result = _userManager.FindByNameAsync(paramUserName).Result;

            // If we could not find the user, throw an exception
            if (result == null) throw new Exception("Could not find the User");

            objExpandedUserDTO.UserName = result.UserName;
            objExpandedUserDTO.Email = result.Email;
            objExpandedUserDTO.PhoneNumber = result.PhoneNumber;

            return objExpandedUserDTO;
        }
        #endregion

        #region private ExpandedUserDTO UpdateDTOUser(ExpandedUserDTO objExpandedUserDTO)
        private async Task<ExpandedUserDTO> UpdateDTOUser(ExpandedUserDTO paramExpandedUserDTO)
        {
            ApplicationUser result =
                _userManager.FindByNameAsync(paramExpandedUserDTO.UserName).Result;

            // If we could not find the user, throw an exception
            if (result == null)
            {
                throw new Exception("Could not find the User");
            }

            result.Email = paramExpandedUserDTO.Email;

            // Lets check if the account needs to be unlocked
            if (_userManager.IsLockedOutAsync(result).Result)
            {
                // Unlock user
                await _userManager.ResetAccessFailedCountAsync(result);
            }

            await _userManager.UpdateAsync(result);

            // Was a password sent across?
            if (!string.IsNullOrEmpty(paramExpandedUserDTO.Password))
            {
                // Remove current password
                var removePassword = _userManager.RemovePasswordAsync(result);
                if (removePassword.IsCompletedSuccessfully)
                {
                    // Add new password
                    var AddPassword =
                        _userManager.AddPasswordAsync(
                            result,
                            paramExpandedUserDTO.Password
                            );

                    if (AddPassword.IsFaulted)
                    {
                        throw new Exception(AddPassword.Exception.Message);
                    }
                }
            }

            return paramExpandedUserDTO;
        }
        #endregion


        #region private UserAndRolesDTO GetUserAndRoles(string UserName)
        private UserAndRolesDTO GetUserAndRoles(string UserName)
        {
            // Go get the User
            ApplicationUser user = _userManager.FindByNameAsync(UserName).Result;

            List<UserRoleDTO> colUserRoleDTO =
                (from objRole in _userManager.GetRolesAsync(user).Result
                 select new UserRoleDTO
                 {
                     RoleName = objRole,
                     UserName = UserName
                 }).ToList();

            if (colUserRoleDTO.Count() == 0)
            {
                colUserRoleDTO.Add(new UserRoleDTO { RoleName = "Ролей не найдено" });
            }

            ViewBag.AddRole = new SelectList(RolesUserIsNotIn(UserName));

            // Create UserRolesAndPermissionsDTO
            UserAndRolesDTO objUserAndRolesDTO = new UserAndRolesDTO();
            objUserAndRolesDTO.UserName = UserName;
            objUserAndRolesDTO.colUserRoleDTO = colUserRoleDTO;
            return objUserAndRolesDTO;
        }
        #endregion

        #region private List<string> RolesUserIsNotIn(string UserName)
        private List<string> RolesUserIsNotIn(string UserName)
        {
            // Get roles the user is not in
            var colAllRoles = _roleManager.Roles.Select(x => x.Name).ToList();

            // Go get the roles for an individual
            ApplicationUser user = _userManager.FindByNameAsync(UserName).Result;

            // If we could not find the user, throw an exception
            if (user == null)
            {
                throw new Exception("Пользователь не найден");
            }

            var colRolesForUser = _userManager.GetRolesAsync(user).Result.ToList();
            var colRolesUserInNotIn = (from objRole in colAllRoles
                                       where !colRolesForUser.Contains(objRole)
                                       select objRole).ToList();

            if (colRolesUserInNotIn.Count() == 0)
            {
                colRolesUserInNotIn.Add("Ролей не найдено");
            }

            return colRolesUserInNotIn;
        }
        #endregion

        [AllowAnonymous]
        public void CreateAdmin()
        {

            if (!_roleManager.RoleExistsAsync
            ("Administrator").Result)
            {
                var role = new IdentityRole();
                role.Name = "Administrator";
                IdentityResult roleResult = _roleManager.
                CreateAsync(role).Result;
            }

            if (!_roleManager.RoleExistsAsync
            ("Operator").Result)
            {
                var role = new IdentityRole();
                role.Name = "Operator";
                IdentityResult roleResult = _roleManager.
                CreateAsync(role).Result;
            }


            if (!_roleManager.RoleExistsAsync
            ("Manager").Result)
            {
                var role = new IdentityRole();
                role.Name = "Manager";
                IdentityResult roleResult = _roleManager.
                CreateAsync(role).Result;
            }

            if (_userManager.FindByNameAsync
            ("Admin").Result == null)
            {
                var user = new ApplicationUser();
                user.UserName = "Admin";
                user.Email = "Admin@localhost";
                user.External = false;
                IdentityResult result = _userManager.CreateAsync
                (user, "Pass1!").Result;

                if (result.Succeeded)
                {
                    _userManager.AddToRoleAsync(user, "Administrator").Wait();
                }
            }
            if (_userManager.FindByNameAsync
            ("Manager").Result == null)
            {
                var user = new ApplicationUser();
                user.UserName = "Manager";
                user.Email = "Manager@localhost";
                user.External = false;
                IdentityResult result = _userManager.CreateAsync
                (user, "Pass1!").Result;

                if (result.Succeeded)
                {
                    _userManager.AddToRoleAsync(user, "Manager").Wait();
                }
            }

            if (_userManager.FindByNameAsync
            ("Operator").Result == null)
            {
                var user = new ApplicationUser();
                user.UserName = "Operator";
                user.Email = "Operator@localhost";
                user.External = false;
                IdentityResult result = _userManager.CreateAsync
                (user, "Pass1!").Result;

                if (result.Succeeded)
                {
                    _userManager.AddToRoleAsync(user, "Operator").Wait();
                }
            }

            if(!_db.Tariffs.Any())
            {
                var newTariffs = new List<Tariff>()
                {
                    new Tariff()
                    {
                        Name = "Free Plan",
                        IsDeleted = false,
                        Months = 3,
                        Price = 0
                    },
                    new Tariff()
                    {
                        Name = "Starter Plan",
                        IsDeleted = false,
                        Months = 6,
                        Price = 19
                    },
                    new Tariff()
                    {
                        Name = "Business Plan",
                        IsDeleted = false,
                        Months = 9,
                        Price = 29
                    },
                    new Tariff()
                    {
                        Name = "Ultimate Plan",
                        IsDeleted = false,
                        Months = 12,
                        Price = 49
                    },
                };
                _db.AddRange(newTariffs);
                _db.SaveChanges();
            }
        }
    }
}