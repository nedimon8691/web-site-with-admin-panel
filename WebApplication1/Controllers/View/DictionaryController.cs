﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class DictionaryController : Controller
    {
        public async Task<ActionResult> ClientConfiguration()
        {
            return View();
        }
        public async Task<ActionResult> ClientConfigurationOperator()
        {
            return View();
        }
        public IActionResult Branch()
        {
            return View();
        }
        public IActionResult BusinessUnit()
        {
            return View();
        }

        public IActionResult RegstrationState()
        {
            return View();
        }

        public IActionResult Classifications()
        {
            return View();
        }

        public IActionResult QuestionCategory()
        {
            return View();
        }

        public IActionResult Questions()
        {
            return View();
        }


        public IActionResult Tests()
        {
            return View();
        }

        public IActionResult ClassificationSub()
        {
            return View();
        }

        public IActionResult DeclarerType()
        {
            return View();
        }

        public IActionResult DeclarerRole()
        {
            return View();
        }

        public IActionResult QuestionCategorySub()
        {
            return View();
        }
        public IActionResult Tariffs()
        {
            return View();
        }
    }
}
