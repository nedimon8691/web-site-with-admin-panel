﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models.Dictionary;
namespace WebApplication1.Controllers
{
    [Authorize]
    public class AuditActionPlanController : Controller
    {
        public IActionResult AllActionPlan()
        {
            ViewBag.States = StatesDic.GetAllStatuses();
            return View();
        }
        public IActionResult CompletedActionPlan()
        {
            ViewBag.States = StatesDic.GetAllStatuses();
            return View();
        }

        public IActionResult NotCompletedActionPlan()
        {
            ViewBag.States = StatesDic.GetAllStatuses();
            return View();
        }

        public IActionResult OverdueActionPlan()
        {
            ViewBag.States = StatesDic.GetAllStatuses();
            return View();
        }

        public IActionResult DataImport()
        {
            return View();
        }
    }
}
