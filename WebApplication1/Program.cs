using Microsoft.OData.ModelBuilder;
using WebApplication1.Models.AuditModels;
using WebApplication1.Models.QuestionsAndAnswers;
using Microsoft.OData.Edm;
using Newtonsoft.Json.Serialization;
using WebApplication1.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using WebApplication1.Models.IdentityModels;
using WebApplication1.Services.AuditServices;
using WebApplication1.Services.EncryptionServices;
using Microsoft.AspNetCore.OData;
using Microsoft.AspNetCore.HttpOverrides;
//using WebApplication.Services.NotificationsServices;

namespace WebApplication1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);


            builder.Services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(
                    builder.Configuration.GetConnectionString("DefaultConnection")));

            builder.Services.AddIdentity<ApplicationUser, IdentityRole>()
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();
            builder.Services.AddTransient<IAuditService, AuditService>();
            //builder.Services.AddTransient<ISendEmail, SendEmail>();
            builder.Services.AddTransient<IEncryptionService, EncryptionService>();
            builder.Services.AddHttpContextAccessor();
            builder.Services.AddControllers();
            builder.Services.AddODataQueryFilter();
            builder.Services.AddControllersWithViews()
                .AddOData(opt => {
                    opt.AddRouteComponents("odata", GetEdmModel());
                    opt.Select().Expand().Filter().OrderBy().SetMaxTop(null).Count();
                })
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null)
                .AddNewtonsoftJson(option => option.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .AddNewtonsoftJson(option => option.SerializerSettings.ContractResolver = new DefaultContractResolver());

            builder.Services.AddRazorPages()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);

            var app = builder.Build();


            if (builder.Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.Use(async (context, next) =>
            {
                if (context.Request.Path == "/")
                    context.Response.Redirect("/Home/Index");
                await next();
                if (context.Response.StatusCode == 404)
                {
                    context.Response.Redirect("/Error/NotFound");
                }
                else if (context.Response.StatusCode == 403)
                {
                    context.Response.Redirect("/Error/AccessDenied");
                }
                else if (context.Response.StatusCode == 401)
                {
                    context.Response.Redirect("/Error/Unauthorized");
                }
                else if (context.Response.StatusCode == 500)
                {
                    context.Response.Redirect("/Error/InternalServerError");
                }
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action}/{id?}",
                    new { controller = "Home", action = "Index" });
                endpoints.MapRazorPages();
            });
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                    ForwardedHeaders.XForwardedProto
            });
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //    endpoints.MapRazorPages();
            //});
            app.Run();
        }

        public static IEdmModel GetEdmModel()
        {
            var odataBuilder = new ODataConventionModelBuilder();
            odataBuilder.EntitySet<AuditData>("AuditDatas");
            odataBuilder.EntitySet<Client>("Clients");
            odataBuilder.EntitySet<ClientConfiguration>("ClientConfigurations");
            odataBuilder.EntitySet<Tariff>("Tariffs");
            odataBuilder.EntitySet<FileModel>("FileModels");
            odataBuilder.EntitySet<ClientConfigurationFileModel>("ClientConfigurationFileModels");
            //FunctionConfiguration OperatorCompleted = odataBuilder.EntityType<Class>().Collection.Function("Controller");
            //OperatorCompleted.ReturnsFromEntitySet<Class>("Controller");

            return odataBuilder.GetEdmModel();
        }
    }
}
