﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models.AuditModels;
using WebApplication1.Models.IdentityModels;
using WebApplication1.Models.QuestionsAndAnswers;

namespace WebApplication1.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<ApplicationUser> ApplcationUsers { get; set; }
        public DbSet<AuditData> AuditDatas { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<ClientConfiguration> ClientConfigurations { get; set; }
        public DbSet<FileModel> FileModels { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<ClientConfigurationFileModel> ClientConfigurationFileModels { get;  set; }
    }
}
