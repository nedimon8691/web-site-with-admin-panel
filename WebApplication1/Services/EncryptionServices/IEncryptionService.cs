﻿using WebApplication1.Models.QuestionsAndAnswers;

namespace WebApplication1.Services.EncryptionServices
{
    public interface IEncryptionService
    {
        byte[] GetLicenseFile(ClientConfiguration licenseData);
    }
}
