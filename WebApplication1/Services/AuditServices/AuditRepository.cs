﻿using Microsoft.EntityFrameworkCore;
using WebApplication1.Data;
using WebApplication1.Models.AuditModels;

namespace WebApplication1.Services.AuditServices
{

    public class AuditRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public AuditRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<AuditData>> GetAuditDatasAsync()
        {
            var result = await _dbContext.AuditDatas.ToListAsync();
            return result;

        }
        public async Task CreateAuditAsync(DateTime Date, string UserName, string IPAddress,
         string ObjectName, string OperationName, bool Success)
        {

            var newAuditRow = new AuditData
            {
                Date = Date,
                UserName = UserName,
                IPAddress = IPAddress,
                ObjectName = ObjectName,
                OperationName = OperationName,
                Success = Success
            };

            await _dbContext.AuditDatas.AddAsync(newAuditRow);
            await _dbContext.SaveChangesAsync();
        }

    }
}
