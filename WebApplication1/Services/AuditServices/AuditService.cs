﻿using WebApplication1.Data;
using WebApplication1.Models.AuditModels;

namespace WebApplication1.Services.AuditServices
{

    public interface IAuditService
    {
         Task CreateAuditASync(DateTime Date, string UserName, string IPAddress,
             string ObjectName, string OperationName, bool Success, string Error);
    }

    public class AuditService : IAuditService
    {
        private readonly ApplicationDbContext _dbContext;

        public AuditService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task CreateAuditASync(DateTime Date, string UserName, string IPAddress, 
            string ObjectName, string OperationName, bool Success, string Error)
        {

            var newAuditRow = new AuditData
            {
                Date = Date,
                UserName = UserName,
                IPAddress = IPAddress,
                ObjectName = ObjectName,
                OperationName = OperationName,
                Success = Success,
                Error = Error
            };

           await _dbContext.AuditDatas.AddAsync(newAuditRow);
           await _dbContext.SaveChangesAsync();
        }
    }
}
