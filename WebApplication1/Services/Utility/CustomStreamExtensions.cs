﻿namespace WebApplication1.Utility
{
    public static class CustomStreamExtensions
    {
        public static byte[] ToArray(this Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];
            var fileStream = stream;
            var array = Array.Empty<byte>();
            using (MemoryStream mems = new MemoryStream())
            {
                int read;
                while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    mems.Write(buffer, 0, read);
                }
                array = mems.ToArray();
            }

            return array;
        }
    }
}
