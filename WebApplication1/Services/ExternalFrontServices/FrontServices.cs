﻿//using WebApplication1.Data;
//using WebApplication1.Models.Dictionary;
////using WebApplication.Models.ExternalApi;

//namespace WebApplication1.Services.ExternalFrontServices
//{
//    public interface IFrontServices
//    {
//        bool UploadFileForReg(IFormFile file, int reg_id);
//    }

//    public class FrontServices : IFrontServices
//    {
//        private ApplicationDbContext _db;
//        private readonly IWebHostEnvironment _appEnvironment;

//        public FrontServices(ApplicationDbContext db, IWebHostEnvironment appEnvironment)
//        {
//            _db = db;
//            _appEnvironment = appEnvironment;
//        }

//        //public List<Tests> Test()
//        //{
//        //    return null;
//        //}

//        public bool UploadFileForReg(IFormFile file, int reg_id)
//        {
//            try
//            {
//                var result = Upload_file(file, reg_id);
//                if (result)
//                    return true;
//                else return false;
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//        }

//        public bool Upload_file(IFormFile file, int reg_id)
//        {
//            int year = DateTime.Now.Year;
//            var folderpath = _appEnvironment.WebRootPath + "/Files/" + year + "/";
//            var innerfolder = _appEnvironment.WebRootPath + "/Files/" + year + "/" + reg_id + "/";
//            var path = Path.Combine(innerfolder, file.FileName);

//            if (!Directory.Exists(folderpath))
//            {
//                Directory.CreateDirectory(folderpath);
//            }
//            if (!Directory.Exists(innerfolder))
//            {
//                Directory.CreateDirectory(innerfolder);
//            }
//            if (file == null)
//            {
//                return false;//BadRequest(Status.Error422("Отсутствует файл, либо загрузка не удалась"));
//            }
//            else
//            {
//                // путь к папке Files
//                // сохраняем файл в папку Files в каталоге wwwroot
//                using (var fileStream = new FileStream(path, FileMode.Create))
//                {
//                    file.CopyTo(fileStream);
//                }
//                var Report = _db.Registration.Find(reg_id);
//                var fileobject = new FileURL()
//                {
//                    filename = file.FileName,
//                    full_path = "/Files/" + year + "/" + reg_id + "/" + file.FileName,
//                    RegistrationID = Report.Id
//                };

//                try
//                {
//                    if (Report.Files == null)
//                    {
//                        Report.Files = new List<FileURL>() { fileobject };
//                        _db.Update(Report);
//                        _db.SaveChanges();
//                    }
//                    else if (_db.FileURLs.FirstOrDefault(f => f.deleted != true && f.RegistrationID == Report.Id && f.filename == fileobject.filename) == null)
//                    {
//                        Report.Files.Add(fileobject);
//                        _db.Update(Report);
//                        _db.SaveChanges();
//                    }
//                    return true;
//                }
//                catch (Exception ex)
//                {
//                    return false;
//                }
//            }
//        }
//    }
//}
