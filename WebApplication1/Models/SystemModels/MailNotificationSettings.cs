﻿namespace WebApplication1.Models.SystemModels
{
    public class MailNotificationSettings
    {
        public string Server { get; set; }
        public string Port { get; set; }
        public string Domain { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
