﻿using Core.Models;

namespace WebApplication1.Models.AuditModels
{
    public class AuditData : BaseEntity
    {
        public DateTime? Date { get; set; }
        public string? UserName { get; set; }
        public string? IPAddress { get; set; }
        public string? ObjectName { get; set; }
        public string? OperationName { get; set; }
        public bool? Success { get; set; }
        public string? Error { get; set; }
    }
}
