﻿namespace Core.Models
{
    public interface IBaseEntity : IDeletableEntity
    {
        int Id { get; set; }
    }
}
