﻿namespace Core.Models
{
    public interface IDeletableEntity
    {
        public bool IsDeleted { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string DeletedBy { get; set; }
    }
}
