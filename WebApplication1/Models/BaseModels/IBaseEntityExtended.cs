﻿namespace Core.Models
{
    public interface IBaseEntityExtended : IBaseEntity
    {
        string Name { get; set; }
        string Desc { get; set; }
    }
}
