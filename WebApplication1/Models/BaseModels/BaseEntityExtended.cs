﻿namespace Core.Models
{

    public class BaseEntityExtended : BaseEntity, IBaseEntityExtended
    {
        public string Name { get; set; }
        public string Desc { get; set; }
    }
}
