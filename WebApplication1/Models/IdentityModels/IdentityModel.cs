﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models.IdentityModels
{
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "BirthDate")]
        [DataType(DataType.Date)]
        public DateTime? BirthDate { get; set; }
        public string? FirstName { get; set; }
        public string? SecondName { get; set; }
        [DisplayFormat(DataFormatString = "{Choose File}", ApplyFormatInEditMode = true)]
        public string? Picture { get; set; }
        public bool? Deleted { get; set; }
        public DateTime? DeletedAt { get; set; }
        public string? AccessToken { get; set; }
        public bool? External { get; set; }

    }
}
