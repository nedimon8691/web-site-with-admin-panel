﻿using Core.Models;

namespace WebApplication1.Models.QuestionsAndAnswers
{
    public class Client : BaseEntity
    {
        public string? Name { get; set; }
        public string? ClientNumber { get; set; }
        public string? AccessLevel { get; set; }
        public DateTime? EntryDate { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? PassHash { get; set; }
        public bool? Appproved { get; set; }
        public virtual ICollection<ClientConfiguration>? ClientConfigurations { get; set; }
    }
}
