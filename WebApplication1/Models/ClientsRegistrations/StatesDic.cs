﻿namespace WebApplication1.Models.Dictionary
{
    public class StatesDic
    {
        public static string GetStatus(int id)
        {
            string[] Statuses = { "New", "Executing", "Completed", "Expired", "Fatal" }; ;
            return Statuses[id];
        }
        public static List<string> GetAllStatuses()
        {
            string[] Statuses = { "New", "Executing", "Completed", "Expired", "Fatal" }; ;
            return Statuses.ToList();
        }
    }
}
