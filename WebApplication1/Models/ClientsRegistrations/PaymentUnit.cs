﻿using WebApplication1.Models.QuestionsAndAnswers;


namespace WebApplication1.DtoModels
{
    public class PaymentUnit
    {
        public string Name { get; set; }
        public string DeviceId { get; set; }
        public int TariffId { get;set; }
        public Tariff Tariff { get; set; }
    }
}
