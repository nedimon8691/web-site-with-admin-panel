﻿namespace WebApplication1.Models.QuestionsAndAnswers
{
    public enum PaymentStatus
    {
        Initial,
        Waiting,
        Completed,
        Failed
    }
}
