﻿using Core.Models;

namespace WebApplication1.Models.QuestionsAndAnswers
{
    public class ClientConfiguration : BaseEntity
    {
        public int? ClientId { get; set; }
        public Client? Client { get; set; }
        public DateTime? DateEnd { get; set; }
        public string? DeviceId { get; set; }
        public ICollection<ClientConfigurationFileModel>? InvoiceFiles { get; set; }
        public float? Price { get; set; }
        public PaymentStatus? PaymentStatus { get; set; }
    }
}
