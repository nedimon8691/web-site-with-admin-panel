﻿using Core.Models;

namespace WebApplication1.Models.QuestionsAndAnswers
{
    public class FileModel : BaseEntity
    {
        public string? FileName { get; set; }
        public byte[]? FileContent { get; set; }
    }
}
