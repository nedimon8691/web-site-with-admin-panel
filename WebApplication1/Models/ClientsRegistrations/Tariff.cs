﻿using Core.Models;

namespace WebApplication1.Models.QuestionsAndAnswers
{
    public class Tariff:BaseEntity
    {
        public string? Name { get;set; }
        public int? Months { get; set; }
        public float? Price { get; set; }
        public int? UserCount { get;set; }
    }
}
