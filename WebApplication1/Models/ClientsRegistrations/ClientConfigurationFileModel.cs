﻿using Core.Models;

namespace WebApplication1.Models.QuestionsAndAnswers
{
    public class ClientConfigurationFileModel : BaseEntity
    {
        public int? FileModelId { get; set; }
        public virtual FileModel? FileModel { get; set; }
        public int? ClientConfigurationId { get; set; }
        public ClientConfiguration? ClientConfiguration { get; set; }
        //public string? OwnerId { get; set; }
        //public ApplicationUser? Owner { get; set; }
        
    }
}
